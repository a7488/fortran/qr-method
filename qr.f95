program qr
    use mod1
    implicit none
    real, allocatable :: A(:, :), Q(:, :), R(:, :)
    real, allocatable :: lambda(:)
    real eps, lambda_im
    integer i, n, j, k
    

    open(1, file = "input") 
    open(2, file = "result") 


    read(1, *) n
    read(1, *) eps
    allocate(A(1:n, 1:n), Q(1:n, 1:n), R(1:n, 1:n), lambda(1:n))
    
    
    do i = 1, n
        read(1, *) (A(i, j), j = 1, n)
    end do
       
    write(2, *) "Исходная матрица А:"

    do i = 1, n
        write(2, *) (A(i, j), j = 1, n)
    end do
    
    do k = 1, 8
        call Haushold(A, n, Q, R)
        A = matmul(R, Q)
        
    enddo

    write(2, *) "__________________________________________________"
    write(2, *) " Точность eps=",eps
    write(2, *) "__________________________________________________"
    write(2, *) " Матрица А после QR итераций"
    do i = 1, n
        write(2, *) (A(i,j), j = 1, n)
    end do
   

    lambda(1) = A(1, 1)
    write(2, *) "__________________________________________________"
    if ((A(2, 2) + A(3, 3))**2 - 4*(A(2, 2)*A(3, 3) - A(2, 3)*A(3, 2)) < 0  ) then
        lambda(2) = (A(2, 2) + A(3, 3) )/ 2 
        lambda_im = - sqrt( - (A(2, 2) + A(3, 3))**2 + 4*(A(2, 2)*A(3, 3) - A(2, 3)*A(3, 2) ) ) / 2

        write(2, *) "Собственные числа матрицы А:"
    
        write(2, *) "Вещественное", lambda(1)
        write(2, *) "Комплексное (",lambda(2),",",lambda_im,")"
        write(2, *) "Комплексное (",lambda(2),",",-lambda_im,")"
    endif

    !write(2, *) (A(2, 2) + A(3, 3))
    !write(2, *)  (A(2, 2) + A(3, 3))**2 - 4*(A(2, 2)*A(3, 3) - A(2, 3)*A(3, 2))

    If ((A(2, 2) + A(3, 3))**2 - 4*(A(2, 2)*A(3, 3) - A(2, 3)*A(3, 2)) >= 0 ) then
        lambda(2) = ( (A(2, 2) + A(3, 3)) - sqrt( (A(2, 2) + A(3, 3))**2 - 4*(A(2, 2)*A(3, 3) - A(2, 3)*A(3, 2) ) ) ) / 2
        lambda(3) = ( (A(2, 2) + A(3, 3)) + sqrt(  (A(2, 2) + A(3, 3))**2 - 4*(A(2, 2)*A(3, 3) - A(2, 3)*A(3, 2) ) ) )/ 2
        write(2, *) "Собственные числа матрицы А:"
        do i = 1, n
            write(2, *) lambda(i)
        enddo
    endif
    


    
     
    
    
end program qr