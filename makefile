com=gfortran
ras=f95
files=$(wildcard *.$(ras))
qr : $(patsubst %.$(ras), %.o, $(files))
	 $(com) $^ -o $@
%.o : %.$(ras) mod1.mod
	 $(com) -c $<
mod1.mod : mod1.f95
	 $(com) -c $<

