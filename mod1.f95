module mod1
    implicit none
    contains

    subroutine Haushold(A, n, Q, R)
        real, allocatable ::  A(:, :), Q(:, :), v(:, :), H(:, :), E(:, :), R(:, :),  zn(:, :), A1(:, :)
        integer i, n, j, k, l, m
        real znam

        allocate( E(1:n, 1:n), v(1:n, 1), H(1:n, 1:n),   zn(1:n, 1:n), A1(1:n, 1:n))

        Q = 1
        E = 1

        A1 = A
               
        do i = 1, n
            do j = 1, n
                if (Q(i,j) == 1 .and. E(i, j) == 1 .and. i /= j) then       !делаю единичными
                    Q(i, j) = 0; E(i, j) = 0
                endif
            enddo
        enddo
      


        do j = 1, n - 1

            if (j >= 2) then
                do k = 1, j - 1
                    v(k, 1) = 0
                enddo
            endif

            if ( A(j,j)>=0 ) then 
                v(j, 1) = A(j, j) + sqrt(sum((A( j:n, j)**2))) 
            else 
                v(j, 1) = A(j, j) - sqrt(sum((A( j:n, j)**2)))
            endif

            do i = j + 1, n
                v(i, 1) = A(i, j)
            enddo

            !write(2, *) "v:", v
            zn = matmul(transpose(v), v)
            znam = zn(1, 1)   


            H = E - 2 * matmul(v, transpose(v)) / znam
            !write(2, *) "H"
            !do i = 1 , n
            !    write(2, *) (H(i, k), k = 1, n)
            !enddo
            
            A = matmul(H, A)   

            !write(2, *) "__________________________________________________"
            !write(2, *) "матрица А на", j,"ой итерации"

            !do i = 1 , n
            !    write(2, *) (A(i, k), k = 1, n)
            !enddo
            
            Q = matmul(Q, H)

            if (j == n - 1) then
                R = A
            endif
           
        enddo
        !write(2, *) " Матрица А После QR разложения"
        !A = matmul(Q, R)
        !do i = 1, n
        !   write(2, *) (A(i,j), j = 1, n)
        !end do
        !write(2, *) "__________________________________________________"

        !write(2, *) " Матрица Q"
        !do i = 1, n
        !    write(2, *) (Q(i,j), j = 1, n)
        !end do
        !write(2, *) "__________________________________________________"
        !write(2, *) " Матрица R"
        !do i = 1, n
        !    write(2, *) (R(i,j), j = 1, n)
        !end do
    
        !A = A1
        
        
        

    end subroutine Haushold


    
end module mod1